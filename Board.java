public class Board
{
	//Kathleen is the name of the first die and Darlene is the second die
	private Die Kathleen;
	private Die Darlene;
	private boolean[] tiles;
	
	public Board()
	{
		Kathleen = new Die();
		Darlene = new Die();
		tiles = new boolean[12];
	}
	
	public String toString()
	{
		//the xMen variable contains the values of the array
		String xMen = "";
		for(int i = 0; i < tiles.length; i++){
			if(tiles[i] == false){
				xMen += (i + 1);
			}else{ 
				xMen += "X";
			}
		}
		return xMen;		
	}
	public boolean playATurn()
	{
		Kathleen.roll();
		Darlene.roll();
		
		System.out.println(Kathleen);
		System.out.println(Darlene);
		
		int sumOfDice = (Kathleen.getFaceValue() + Darlene.getFaceValue());
		
		if(tiles[sumOfDice - 1] == false){
			tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}else if(tiles[Kathleen.getFaceValue() - 1] == false){
			tiles[Kathleen.getFaceValue() - 1] = true;
			System.out.println("Closing tile equal to sum: " + Kathleen.getFaceValue());
			return false;	
		}else if(tiles[Darlene.getFaceValue() - 1] == false){
			tiles[Darlene.getFaceValue() - 1] = true;
			System.out.println("Closing tile equal to sum: " + Darlene.getFaceValue());
			return false;	
		}else{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}

	
}
